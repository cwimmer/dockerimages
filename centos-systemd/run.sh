#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

SYSTEM=`uname -s`

if [ $SYSTEM = 'Darwin' ]
then
    SYS_ADMIN='--cap-add SYS_ADMIN'
    CGROUP='--tmpfs /sys/fs/cgroup'
else
    SYS_ADMIN=''
    CGROUP=`mount | awk '/\/sys\/fs\/cgroup/ && ! /systemd/ { print "-v="$3":"$3":ro" ; }'`
fi

docker run -dt $SYS_ADMIN \
  $CGROUP \
  --tmpfs /run \
  --name centos-systemd \
  cwimmer/centos-systemd \
  "$@"

echo "INFO: Use command \"docker logs centos-systemd\" to see logs of container."
